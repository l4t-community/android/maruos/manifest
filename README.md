# Switchroot Maru OS Manifest

## Getting Started

To get started with Android/Maru OS, you'll need to get familiar with
[Repo](https://source.android.com/source/using-repo.html) and [Version Control
with Git](https://source.android.com/source/version-control.html).

To initialize your local repository using the Maru OS trees, use a command like this:

    repo init -u https://gitlab.com/switchroot-maru-os/manifest -b master --no-clone-bundle --depth=1

Then to sync up:

    repo sync

## Patching

* Repopick topic `icosa-bt-lineage-17.1`
* Repopick commits `286019`, `270702`, `299588`, `307993`, and `309697` off Lineage Gerrit
* Apply all patches in `patches/` to their respective directories based on the format `path_to_patch-description.patch`

## Roadmap

* ~~Get working build~~
* ~~Move Q `foster_tab` beyonder patch to `icosa_sr`~~
* ~~Set up GitLab CI~~
* ~~Work most patches into overlays and forks~~
* ~~Verify Nvidia enhancements are functional~~
* ~~Test HWC~~--borked as expected, use software rendering
* Fix bugs
* Develop and integrate custom UI
* Set up OTA updates
* Final build

## Important details

Uses customized frameworks/base, frameworks/native, and build/make repos with built in Nvidia enhancements support

Device support:
* Nintendo Switch (`maru_icosa_sr`)
* More to come!

## Contributing

See the [main Maru OS repository](https://github.com/maruos/maruos) for more
info.

## Licensing

[Apache 2.0](LICENSE)
